package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
	"google.golang.org/appengine"
	
)

var router *chi.Mux
var db *sql.DB

// Company details struct
type Company struct {
	ID             int
	FakeName       string `json:"fake_company_name"`
	Description    string `json:"description"`
	Tagline        string `json:"tagline"`
	CompanyEmail   string `json:"company_email"`
	BusinessNumber string `json:"business_number"`
	Restricted     string `json:"restricted"`
}

func init() {
	router = chi.NewRouter()
	router.Use(middleware.Recoverer)

	var err error

	if err := godotenv.Load(); err != nil {
		log.Println("File .env not found, reading configuration from ENV")
	}

	// dbHost := os.Getenv("DB_Host")
	// dbPort := os.Getenv("DB_Port")
	socketDir, isSet := os.LookupEnv("DB_SOCKET_DIR")
	if !isSet {
		socketDir = "/cloudsql"
	}

	dbUser                 := os.Getenv("DB_USER")          
	dbPwd                  := os.Getenv("DB_PASS")           
	instanceConnectionName := os.Getenv("INSTANCE_CONNECTION_NAME")
	dbName                 := os.Getenv("DB_NAME")                 


	dbSource := fmt.Sprintf("%s:%s@cloudsql(/%s/%s)/%s?parseTime=true", dbUser, dbPwd, socketDir, instanceConnectionName, dbName)

	db, err = sql.Open("mysql", dbSource)
	if err != nil {
		// return fmt.Errorf("sql.Open: %v", err)
		catch(err)
		log.Println("Error in database connection")
	}

	log.Println(db)

	// dbSource := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8", dbUser, dbPass, dbHost, dbPort, dbName)

	// db, err = sql.Open("mysql", dbSource)
	// catch(err)
}
func paginate(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// just a stub.. some ideas are to look at URL query params for something like
		// the page number, or the limit, and send a query cursor down the chain
		next.ServeHTTP(w, r)
	})
}

func routers() *chi.Mux {

	// router.Handle("/", http.FileServer(http.Dir("./static")))
	router.Get("/", ping)

	router.With(paginate).Get("/company", AllCompanys)
	router.Get("/company/{id}", DetailCompany)
	router.Get("/restricted", RestrictedCompanys)

	return router
}

//-------------- API ENDPOINT ------------------//
// server starting point
func ping(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	respondwithJSON(w, http.StatusOK, map[string]string{"message": "Welcome to Company status check"})
}

// AllCompanys get all the companys
func AllCompanys(w http.ResponseWriter, r *http.Request) {
	errors := []error{}
	payload := []Company{}

	rows, err := db.Query("Select * From faux_id_fake_companies")
	catch(err)

	defer rows.Close()

	for rows.Next() {
		data := Company{}
		// inda := IndustryString{}

		er := rows.Scan(
			&data.ID,
			&data.FakeName,
			&data.Description,
			&data.Tagline,
			&data.CompanyEmail,
			&data.BusinessNumber,
			&data.Restricted,
		)

		// data.Industry = strings.Split(inda.Industry, ",")

		// fmt.Println(data)

		if er != nil {
			errors = append(errors, er)
		}
		payload = append(payload, data)
	}
	w.Header().Set("Access-Control-Allow-Origin", "*")

	respondwithJSON(w, http.StatusOK, payload)
}

// DetailCompany get specific company details
func DetailCompany(w http.ResponseWriter, r *http.Request) {
	payload := Company{}
	// inda := IndustryString{}

	id := chi.URLParam(r, "id")
	// fmt.Println(id)

	row := db.QueryRow("Select * From faux_id_fake_companies where `business_number`=?", id)

	err := row.Scan(
		&payload.ID,
		&payload.FakeName,
		&payload.Description,
		&payload.Tagline,
		&payload.CompanyEmail,
		&payload.BusinessNumber,
		&payload.Restricted,
	)

	// payload.Industry = strings.Split(inda.Industry, ",")

	if err != nil {
		respondWithError(w, http.StatusNotFound, "no rows in result set")
		return
	}
	w.Header().Set("Access-Control-Allow-Origin", "*")
	respondwithJSON(w, http.StatusOK, payload)
}

// RestrictedCompanys get all the companys
func RestrictedCompanys(w http.ResponseWriter, r *http.Request) {
	errors := []error{}
	payload := []Company{}

	rows, err := db.Query("SELECT * FROM `faux_id_fake_companies` WHERE `restricted` = 'Yes'")
	catch(err)

	defer rows.Close()

	for rows.Next() {
		data := Company{}
		// inda := IndustryString{}

		er := rows.Scan(
			&data.ID,
			&data.FakeName,
			&data.Description,
			&data.Tagline,
			&data.CompanyEmail,
			&data.BusinessNumber,
			&data.Restricted,
		)

		// data.Industry = strings.Split(inda.Industry, ",")

		// fmt.Println(data)

		if er != nil {
			errors = append(errors, er)
		}
		payload = append(payload, data)
	}
	w.Header().Set("Access-Control-Allow-Origin", "*")
	respondwithJSON(w, http.StatusOK, payload)
}

func main() {
	routers()
	if err := godotenv.Load(); err != nil {
		log.Println("File .env not found, reading port configuration from ENV")
	}

	port := os.Getenv("PORT")
	if port == "" {
		port = "8090"
	}
	http.ListenAndServe(":"+port, Logger())
	appengine.Main() // Start the server

}
